# registry

![Version: 0.0.0-dev](https://img.shields.io/badge/Version-0.0.0--dev-informational?style=flat-square) ![AppVersion: SERVICE-VERSION](https://img.shields.io/badge/AppVersion-SERVICE--VERSION-informational?style=flat-square)

Ryax internal container registry.

**Homepage:** <https://ryax.tech>

## Source Code

* <https://gitlab.com/ryax-tech/ryax/common-helm-charts>

## Values

### Global

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| global.nodeSelector | object | `{}` | Add nodeSelector injected as-is (https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/#nodeselector) |

### Resource Settings

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| resources | object | `{}` | Recommended resource requirement Example:  resources:    requests:      memory: "500Mi"      cpu: "100m"    limits:      memory: "750Mi" |

### Other Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| credentials.enabled | bool | `true` | Enable credentials, disable this to make your registry public |
| credentials.htpasswd | string | `"ryax:$2y$05$HACQWbBSAockz.nOHH0xHOg6uFjjDJEuCIzA4Di.VF5eFy2/NETIy"` | This is a hash list of credentials as provided by htpasswd WARNING: Change this in production, this is unsecure ! To create new credentials run:   htpasswd -Bbn ryax "ryaxPassw0rd?" |
| credentials.injectInNamespaces | list | `["default","example"]` | Namepace where the registry pull secret should be injected |
| credentials.password | string | `"ryaxPassw0rd?"` | registry access default password WARNING: Change this in production, this is unsecure ! |
| credentials.pullSecretName | string | `"ryax-registry-pull-secret"` | Name of the pull secret injected in the namespaces |
| credentials.username | string | `"ryax"` | registry access default username WARNING: Change this in production, this is unsecure ! |
| debugPort | int | `5001` | Metrics port used by monitoring |
| garbageCollect.imagePullPolicy | string | `"IfNotPresent"` |  |
| garbageCollect.resources | object | `{}` | Do not add request unless you are sure. The lack of resource it might make the job fail forever... |
| image | string | `"registry:2"` | container image name and version |
| imagePullPolicy | string | `"Always"` |  |
| ingress.annotations | object | `{}` | Custom annotation for the Ingress. Example to add certManager generated certificate: annotations:   cert-manager.io/cluster-issuer: "letsencrypt-production" |
| ingress.certSecret | string | `"ryax-registry-ingress-cert"` | Name of the secret that contains the TLS certificate (not provisionned by this chart) |
| ingress.enabled | bool | `true` | Create a public Ingress to access the repository. *WARNING*: If disabled it is replaced by a self-sign certificate that is injected with DaemonSet on all nodes (Only works with containerd and k3s with mutable config) *WARNING*: kubelet will be able to pull the images only if a valid certificate is associated to the Ingress |
| ingress.hostname | string | `nil` | hostname of the registry required if ingress is enabled Must be the cluster FQDN like "registry.cluster.example.io" use Use to create a valid certificate and to create docker pull credentials |
| monitoring.enabled | bool | `true` |  |
| monitoring.namespace | string | `"ryaxns-monitoring"` |  |
| nodePort | int | `30012` | This is required for the kubelet to pull the image from localhost. This is only used if the ingress is disabled. This is useful in private clusters because image pull is done by the kubelet that do not have access to internal services. Using a node port allows to give acess to any host as localhost. Value in range 30000-32767 |
| priorityClass | string | `"backbone"` |  |
| pullSecret | string | `nil` | pull secret like in pod template |
| pvcSize | string | `"10Gi"` | storage size |
| servicePort | int | `5000` |  |
| storageClass | string | `nil` | leave empty for the default value |

----------------------------------------------
Autogenerated from chart metadata using [helm-docs v1.13.1](https://github.com/norwoodj/helm-docs/releases/v1.13.1)
