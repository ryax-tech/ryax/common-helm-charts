# datastore

![Version: 0.0.0-dev](https://img.shields.io/badge/Version-0.0.0--dev-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: SERVICE-VERSION](https://img.shields.io/badge/AppVersion-SERVICE--VERSION-informational?style=flat-square)

Ryax database

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| databases[0] | string | `"repository"` |  |
| databases[1] | string | `"studio"` |  |
| databases[2] | string | `"authorization"` |  |
| databases[3] | string | `"runner"` |  |
| databases[4] | string | `"worker"` |  |
| datastoreDB | string | `"ryaxdb"` |  |
| datastoreSecret | string | `"ryax-datastore-secret"` |  |
| datastoreUser | string | `"ryax"` |  |
| exporterImage | string | `"quay.io/prometheuscommunity/postgres-exporter:v0.13.2"` |  |
| exporterResources.limits.cpu | string | `"200m"` |  |
| exporterResources.limits.memory | string | `"200Mi"` |  |
| exporterResources.requests.cpu | string | `"100m"` |  |
| exporterResources.requests.memory | string | `"200Mi"` |  |
| global.nodeSelector | object | `{}` |  |
| image | string | `"postgres:13-alpine"` |  |
| monitoring.enabled | bool | `true` |  |
| monitoring.namespace | string | `"ryaxns-monitoring"` |  |
| priorityClass | string | `"backbone"` |  |
| pullSecret | string | `nil` |  |
| pvcName | string | `"ryax-datastore-pvc-0"` |  |
| pvcSize | string | `"2Gi"` |  |
| resources.limits.memory | string | `"1000Mi"` |  |
| resources.requests.cpu | string | `"100m"` |  |
| resources.requests.memory | string | `"500Mi"` |  |
| servicePort | int | `5432` |  |
| storageClass | string | `nil` |  |

----------------------------------------------
Autogenerated from chart metadata using [helm-docs v1.13.1](https://github.com/norwoodj/helm-docs/releases/v1.13.1)
